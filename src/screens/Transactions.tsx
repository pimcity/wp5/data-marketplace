import Box from '../components/Box';
import ButtonLink from '../components/ButtonLink';
import ClosedOffersTable from '../components/offerComponents/ClosedOffersTable';
import OpenOffersTable from '../components/offerComponents/OpenOffersTable';

const Transactions = () => (
    <div className='flex flex-col'>
      <Box heading={{
        left: 'Open Transactions',
        right: (<ButtonLink to='/transactions/create'>Create a New Offer</ButtonLink>),
      }}>
        <OpenOffersTable />
      </Box>

      <Box containerClassName='mt-8' heading='Past Transactions'>
        <ClosedOffersTable />
      </Box>
    </div>
);

export default Transactions;
