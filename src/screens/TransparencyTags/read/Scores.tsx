import Box from '../../../components/Box';
import StarsField from '../../../components/fields/StarsField';
import { Scores as ScoresModel } from '../../../types/transparencyTags';

const Scores = (
  {
    scores,
    className,
  }: { scores: ScoresModel; className?: string },
) => (
    <Box heading='Scores' containerClassName={className}>
      <StarsField name='Privacy score' value={scores.privacyScore} />
      <StarsField name='Transparency score' value={scores.transparencyScore} />
      <StarsField name='Security score' value={scores.securityScore} />
    </Box>
);

export default Scores;
