import Box from '../../../components/Box';
import Button from '../../../components/Button';
import InfoField from '../../../components/fields/InfoField';
import { useModal } from '../../../contexts/modalContext';
import { Webdata as WebdataModel } from '../../../types/transparencyTags';
import WebdataDetails from './WebdataDetails';

const Webdata = (
  {
    webdata,
    className,
  }: { webdata: WebdataModel; className?: string },
) => {
  const { openModal } = useModal();
  return (
    <Box
      heading='Webdata'
      containerClassName={`flex flex-col ${className || ''}`}
      contentClassName='px-6 py-5 flex-1 flex flex-col'
    >
      <div className='flex-1'>
        <InfoField name='Company name' value={webdata.companyName} />
        <InfoField name='Operates under' value={webdata.operatesUnder} />
        <InfoField name='Category' value={webdata.categories} />
        <InfoField name='Rank in category' value={webdata.rankInCategory} />
        <InfoField name='Connected third-party services' value={webdata.connectedThirdParties} />
        <InfoField name='Third Party' value={webdata.thirdParty} />
      </div>

      <div className='flex justify-center w-full'>
        <Button
          className='w-72 h-11'
          theme='alternative'
          onClick={() => openModal(
            <WebdataDetails webdata={webdata} />,
          )}
        >
          View more
        </Button>
      </div>
    </Box>
  );
};

export default Webdata;
