import Box from '../../../components/Box';
import InfoField from '../../../components/fields/InfoField';
import Section from '../../../components/Section';
import { ProvidedInformation as ProvidedInformationModel } from '../../../types/transparencyTags';

const ProvidedInformationDetails = (
  {
    name,
    providedInformation: pi,
    className,
  }: { name: string; providedInformation: ProvidedInformationModel; className?: string },
) => (
    <Box heading={`Information provided by ${name}`} containerClassName={className} contentClassName='px-6 pb-5'>
      <Section heading='General information'>
        <InfoField name='Declared company name' value={pi.declaredCompanyName} labelSize='w-48' />
        <InfoField name='Website' value={pi.website} labelSize='w-48' />
        <InfoField name='Country' value={pi.country} labelSize='w-48' />
        <InfoField name='Category' value={pi.category} labelSize='w-48' />
        <InfoField name='User Rights' value={pi.userRights} labelSize='w-48' />
      </Section>

      <Section heading='Data information'>
        <InfoField name='Data location' value={pi.dataLocation} labelSize='w-48' />
        <InfoField name='Data processed' value={pi.dataProcessed} labelSize='w-48' />
        <InfoField name='Data processing legal basis' value={pi.dataProcessingLegalBasis} labelSize='w-48' />
        <InfoField name='Purpose of data collection' value={pi.dataProcessingPurposes} labelSize='w-48' />
        <InfoField name='Data transfer' value={pi.dataTransfer} labelSize='w-48' />
        <InfoField name='Data for automatic decision' value={pi.dataForAutomaticDecision} labelSize='w-48' />
        <InfoField name='Data recipients' value={pi.dataRecipients} labelSize='w-48' />
        <InfoField name='Data retention period' value={pi.dataRetentionPeriod} labelSize='w-48' />
      </Section>

      <Section heading='Data Controller information'>
        <InfoField name='Data Controller' value={pi.dataController} labelSize='w-48' />
        <InfoField name='Contact address' value={pi.dataControllerContactAddress} labelSize='w-48' />
        <InfoField name='Contact mail' value={pi.dataControllerContactMail} labelSize='w-48' />
        <InfoField name='Contact phone' value={pi.dataControllerContactPhone} labelSize='w-48' />
        <InfoField name='Identity' value={pi.dataControllerIdentity} labelSize='w-48' />
        <InfoField name='Data processor' value={pi.dataProcessor} labelSize='w-48' />
        <InfoField name='Data subprocessors' value={pi.dataSubprocessors} labelSize='w-48' />
        <InfoField name='Consent withdraw' value={pi.consentWithdraw} labelSize='w-48' />
      </Section>

      <Section heading='DPO Information' border='none'>
        <InfoField name='DPO Address' value={pi.dpoContactAddress} labelSize='w-48' />
        <InfoField name='DPO Mail' value={pi.dpoContactMail} labelSize='w-48' />
        <InfoField name='DPO Phone' value={pi.dpoContactPhone} labelSize='w-48' />
      </Section>
    </Box>
);

export default ProvidedInformationDetails;
