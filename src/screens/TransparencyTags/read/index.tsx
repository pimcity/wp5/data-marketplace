import Box from '../../../components/Box';
import ButtonLink from '../../../components/ButtonLink';
import { useGet } from '../../../hooks/useApi';
import useUser from '../../../hooks/useUser';
import { TransparencyTags as TransparencyTagsModel } from '../../../types/transparencyTags';
import { deepSnakeToCamel } from '../../../utils/objects';

import LoadingScreen from '../../LoadingScreen';
import ProvidedInformation from './ProvidedInformation';
import Scores from './Scores';
import Webdata from './Webdata';

const TransparencyTags = () => {
  const user = useUser();

  if (!user) return <LoadingScreen />;

  const { data: tags, isError, error } = useGet<TransparencyTagsModel>(
    `https://privacy-metrics.easypims.eu/privacy-metrics/${user.domain}/`,
    undefined,
    (res) => deepSnakeToCamel(res) as TransparencyTagsModel,
    false,
  );

  const noTags = isError && error && error.res.status === 404;

  if (!tags && !isError) return <LoadingScreen />;

  return (
    <div className='flex flex-col'>
      <Box heading={{
        left: `Transparency Tag for ${user.domain}`,
        right: (!isError && <ButtonLink to='/transparency-tags/edit'>Edit Tags</ButtonLink>),
      }}>
        {isError && !noTags && (
          <div className="flex items-center justify-center w-full my-6 font-bold text-red-500">
            Error fetching Transparency Tags
          </div>
        )}
        {noTags && (
          <div className="flex flex-col items-center justify-center w-full my-6">
            There are no Transparency Tags registered for your company
            <ButtonLink className='mt-6' to='/transparency-tags/create'>Create Tags</ButtonLink>
          </div>
        )}
        {tags && (
          <div className='flex'>
            <ProvidedInformation className='flex-1' name={tags.name} providedInformation={tags.providedInformation} />
            <div className='ml-2'>
              <Webdata webdata={tags.webdata} className='mb-2' />
              <Scores scores={tags.scores} />
            </div>
          </div>
        )}
      </Box>
    </div>
  );
};

export default TransparencyTags;
