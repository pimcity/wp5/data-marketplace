import Box from '../../../components/Box';
import InfoField from '../../../components/fields/InfoField';
import { Webdata as WebdataModel } from '../../../types/transparencyTags';

const WebdataDetails = (
  { webdata }: { webdata: WebdataModel },
) => (
    <Box heading={`Webdata from ${webdata.companyName}`}>
      <InfoField name='Company name' value={webdata.companyName} labelSize='w-48' />
      <InfoField name='Operates under' value={webdata.operatesUnder} labelSize='w-48' />
      <InfoField name='Category' value={webdata.categories} labelSize='w-48' />
      <InfoField name='Rank in category' value={webdata.rankInCategory} labelSize='w-48' />
      <InfoField name='Connected websites' value={webdata.connectedWebsites} labelSize='w-48' />
      <InfoField name='Presence in security lists' value={webdata.presenceInSecurityLists} labelSize='w-48' />
      <InfoField name='Third Party' value={webdata.thirdParty} labelSize='w-48' />
      <InfoField name='Privacy policy' value={webdata.privacyPolicy} labelSize='w-48' />
      <InfoField name='Reach' value={webdata.reach} labelSize='w-48' />
      <InfoField name='Tracking devices' value={webdata.trackingDevices} labelSize='w-48' />
      <InfoField name='Connected third-party services' value={webdata.connectedThirdParties} labelSize='w-48' />
    </Box>
);

export default WebdataDetails;
