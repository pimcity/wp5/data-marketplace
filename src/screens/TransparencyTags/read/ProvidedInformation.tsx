import Box from '../../../components/Box';
import Button from '../../../components/Button';
import InfoField from '../../../components/fields/InfoField';
import { useModal } from '../../../contexts/modalContext';
import { ProvidedInformation as ProvidedInformationModel } from '../../../types/transparencyTags';
import ProvidedInformationDetails from './ProvidedInformationDetails';

interface ProvidedInformationProps {
  name: string;
  providedInformation: ProvidedInformationModel;
  className?: string;
}

const ProvidedInformation = (props: ProvidedInformationProps) => {
  const {
    name,
    providedInformation,
    className,
  } = props;
  const { openModal } = useModal();
  return (
    <Box
      heading={`Information provided by ${name}`}
      containerClassName={`flex flex-col ${className || ''}`}
      contentClassName='px-6 py-5 flex-1 flex flex-col'
    >
      <div className='flex-1'>
        <InfoField name='Declared company name' value={providedInformation.declaredCompanyName} />
        <InfoField name='Website' value={providedInformation.website} />
        <InfoField name='Country' value={providedInformation.country} />
        <InfoField name='Category' value={providedInformation.category} />
        <InfoField name='Purpose of data collection' value={providedInformation.dataProcessingPurposes} />
        <InfoField name='Data controller' value={providedInformation.dataController} />
        <InfoField name='Data processor' value={providedInformation.dataProcessor} />
      </div>

      <div className='flex justify-center w-full'>
        <Button
          className='w-72 h-11'
          theme='alternative'
          onClick={() => openModal(
            <ProvidedInformationDetails name={name} providedInformation={providedInformation} />,
          )}
        >
          View more
        </Button>
      </div>
    </Box>
  );
};

export default ProvidedInformation;
