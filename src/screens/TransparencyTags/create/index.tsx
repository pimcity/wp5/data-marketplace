import { usePost } from '../../../hooks/useApi';
import useUser from '../../../hooks/useUser';
import { TransparencyTagsSubmission, TransparencyTags as TransparencyTagsModel } from '../../../types/transparencyTags';
import { deepSnakeToCamel } from '../../../utils/objects';
import TransparencyTagsForm from '../form';

const CreateTransparencyTags = () => {
  const user = useUser();

  const {
    mutate: submitTags, status,
  } = usePost<Array<TransparencyTagsSubmission>, TransparencyTagsModel>(
    'https://privacy-metrics.easypims.eu/privacy-metrics',
    undefined,
    undefined,
    (res) => deepSnakeToCamel(res) as TransparencyTagsModel,
  );

  const onSubmit = (info: TransparencyTagsSubmission['provided_information']) => {
    submitTags({
      body: [
        {
          identifier: '0',
          name: user?.domain as string,
          provided_information: info,
          webdata: {
            categories: [info.category],
            company_name: info.declared_company_name,
            connected_third_parties: [],
            connected_websites: [info.website],
            operates_under: info.website,
            presence_in_security_lists: [],
            privacy_policy: info.website,
            rank_in_category: 1133,
            reach: 1,
            third_party: false,
            tracking_devices: ['cookies'],
            website: true,
          },
          scores: {
            privacy_score: 5,
            security_score: 5,
            transparency_score: 5,
          },
        },
      ],
    });
  };

  return <TransparencyTagsForm user={user} onSubmit={onSubmit} status={status} />;
};

export default CreateTransparencyTags;
