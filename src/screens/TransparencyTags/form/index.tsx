import {
  FormEvent, useEffect, useRef, useState,
} from 'react';
import { MutationStatus } from 'react-query';
import { useHistory } from 'react-router-dom';

import Box from '../../../components/Box';
import Button from '../../../components/Button';
import InputField from '../../../components/fields/InputField';
import MultiSelectField from '../../../components/fields/MultiSelectField';
import SelectField from '../../../components/fields/SelectField';
import Section from '../../../components/Section';

import countriesOptions from '../../../constants/countries.json';
import dataProcessingPurposesOptions from '../../../constants/dataProcessingPurposes.json';
import { User } from '../../../hooks/useUser';
import { TransparencyTags as TransparencyTagsModel, TransparencyTagsSubmission } from '../../../types/transparencyTags';

const countries = countriesOptions.map((c) => c.label);

interface TransparencyTagsFormProps {
  user?: User;
  onSubmit: (info: TransparencyTagsSubmission['provided_information']) => void;
  status: MutationStatus;
  initialValue?: Partial<TransparencyTagsModel['providedInformation']>;
}

const TransparencyTagsForm = ({
  user, onSubmit, status, initialValue,
}: TransparencyTagsFormProps) => {
  const history = useHistory();
  const [customError, setCustomError] = useState<string | undefined>(undefined);

  // General Information
  const declaredCompanyRef = useRef<HTMLInputElement>(null);
  const websiteRef = useRef<HTMLInputElement>(null);
  const countryRef = useRef<HTMLSelectElement>(null);
  const categoryRef = useRef<HTMLInputElement>(null);
  const userRightsRef = useRef<HTMLInputElement>(null);

  // Data information
  const dataLocationRef = useRef<HTMLSelectElement>(null);
  const dataProcessedRef = useRef<HTMLInputElement>(null);
  const dataProcessingLegalBasisRef = useRef<HTMLInputElement>(null);
  const [dataProcessingPurposes, setDataProcessingPurposes] = useState<Array<string>>(
    initialValue?.dataProcessingPurposes || [],
  );
  const dataTransferRef = useRef<HTMLSelectElement>(null);
  const dataForAutomaticDecisionRef = useRef<HTMLInputElement>(null);
  const dataRecipientsRef = useRef<HTMLInputElement>(null);
  const dataRetentionPeriodRef = useRef<HTMLInputElement>(null);

  // Data Controller information
  const dataControllerRef = useRef<HTMLInputElement>(null);
  const dataControllerContactAddressRef = useRef<HTMLInputElement>(null);
  const dataControllerContactMailRef = useRef<HTMLInputElement>(null);
  const dataControllerContactPhoneRef = useRef<HTMLInputElement>(null);
  const dataControllerIdentityRef = useRef<HTMLInputElement>(null);
  const dataProcessorRef = useRef<HTMLInputElement>(null);
  const dataSubprocessorsRef = useRef<HTMLInputElement>(null);
  const consentWithdrawRef = useRef<HTMLInputElement>(null);

  // DPO Information
  const dpoContactAddressRef = useRef<HTMLInputElement>(null);
  const dpoContactMailRef = useRef<HTMLInputElement>(null);
  const dpoContactPhoneRef = useRef<HTMLInputElement>(null);

  const reset = () => history.push('/transparency-tags');

  const submit = (e: FormEvent) => {
    e.preventDefault();

    if (dataProcessingPurposes.length === 0) setCustomError('Purpose of data collection is required');
    else {
      setCustomError(undefined);
      onSubmit({
        category: categoryRef.current?.value,
        consent_withdraw: consentWithdrawRef.current?.value,
        country: countryRef.current?.value,
        data_controller: dataControllerRef.current?.value,
        data_controller_contact_address: dataControllerContactAddressRef.current?.value,
        data_controller_contact_mail: dataControllerContactMailRef.current?.value,
        data_controller_contact_phone: dataControllerContactPhoneRef.current?.value,
        data_controller_identity: dataControllerIdentityRef.current?.value,
        data_for_automatic_decision: dataForAutomaticDecisionRef.current?.value,
        data_location: dataLocationRef.current?.value,
        data_processed: dataProcessedRef.current?.value,
        data_processing_legal_basis: dataProcessingLegalBasisRef.current?.value,
        data_processing_purposes: dataProcessingPurposes,
        data_processor: dataProcessorRef.current?.value,
        data_recipients: dataRecipientsRef.current?.value.split(',').map((v) => v.trim()),
        data_retention_period: dataRetentionPeriodRef.current?.value,
        data_subprocessors: dataSubprocessorsRef.current?.value.split(',').map((v) => v.trim()),
        data_transfer: dataTransferRef.current?.value,
        declared_company_name: declaredCompanyRef.current?.value,
        dpo_contact_address: dpoContactAddressRef.current?.value,
        dpo_contact_mail: dpoContactMailRef.current?.value,
        dpo_contact_phone: dpoContactPhoneRef.current?.value,
        owner: user?.email,
        user_rights: userRightsRef.current?.value,
        website: websiteRef.current?.value,
      });
    }
  };

  useEffect(() => {
    if (status === 'success') history.push('/transparency-tags');
  }, [status]);

  return (
    <div className='flex flex-col'>
      <Box heading={`Transparency Tag${user?.domain ? ` for ${user.domain}` : ''}`} contentClassName=''>
        <form onReset={reset} onSubmit={submit} className='flex flex-col'>
          <div className='flex flex-col flex-1 px-6 pb-5'>
            <Section heading='General information'>
              <InputField title='Declared company name' inputRef={declaredCompanyRef} labelSize='w-48' required placeholder='Company name as declared by the company.' defaultValue={initialValue?.declaredCompanyName} />
              <InputField title='Website' inputRef={websiteRef} labelSize='w-48' placeholder="Company's official website, e.g. https://example.com" defaultValue={initialValue?.website} />
              <SelectField title='Country' selectRef={countryRef} options={countries} labelSize='w-48' emptyLabel='Select establishment country as declared by the company.' defaultValue={initialValue?.country} />
              <InputField title='Category' inputRef={categoryRef} labelSize='w-48' placeholder='Self-declared categorization information based on business or mission.' defaultValue={initialValue?.category} />
              <InputField title='User Rights' inputRef={userRightsRef} labelSize='w-48' placeholder='Describe the resources where the user can get information to apply rights on her data.' defaultValue={initialValue?.userRights} />
            </Section>

            <Section heading='Data information'>
              <SelectField title='Data location' selectRef={dataLocationRef} options={countries} labelSize='w-48' emptyLabel='Select country where data is going to be physically stored.' defaultValue={initialValue?.dataLocation} />
              <InputField title='Data processed' inputRef={dataProcessedRef} labelSize='w-48' placeholder='Describe which kind of data is collected and processed.' defaultValue={initialValue?.dataProcessed} />
              <InputField title='Data processing legal basis' inputRef={dataProcessingLegalBasisRef} labelSize='w-48' placeholder='Describe whether the processing is based on Consent or Legitimate Interest.' defaultValue={initialValue?.dataProcessingLegalBasis} />
              <MultiSelectField title='Purpose of data collection' value={dataProcessingPurposes} options={dataProcessingPurposesOptions} onSelectValue={setDataProcessingPurposes} labelSize='w-48' placeholder='Select how processed data will be used by the company.' />
              <SelectField title='Data transfer' selectRef={dataTransferRef} options={countries} labelSize='w-48' emptyLabel='Select third country or international organization to transfer personal data' defaultValue={initialValue?.dataTransfer} />
              <InputField title='Data for automatic decision' inputRef={dataForAutomaticDecisionRef} labelSize='w-48' placeholder='Describe whether the data will be used for automatic decision-making and possible consequences for the user.' defaultValue={initialValue?.dataForAutomaticDecision} />
              <InputField title='Data recipients' inputRef={dataRecipientsRef} labelSize='w-48' placeholder='Describe who will receive the data (if any), using comma-separated values, e.g. "Recipient A, Recipient B, Recipient C".' defaultValue={initialValue?.dataRecipients?.join(', ')} />
              <InputField title='Data retention period' inputRef={dataRetentionPeriodRef} labelSize='w-48' placeholder='Describe how long data is going to be stored by the data owner, or the criteria used to determine the period.' defaultValue={initialValue?.dataRetentionPeriod} />
            </Section>

            <Section heading='Data Controller information'>
              <InputField title='Data Controller' inputRef={dataControllerRef} labelSize='w-48' placeholder='Describe who owns the data subject to processing.' defaultValue={initialValue?.dataController} />
              <InputField title='Contact address' inputRef={dataControllerContactAddressRef} labelSize='w-48' placeholder='Physical address to contact the controller.' defaultValue={initialValue?.dataControllerContactAddress} />
              <InputField title='Contact mail' inputRef={dataControllerContactMailRef} labelSize='w-48' placeholder='Mail address to contact the controller.' defaultValue={initialValue?.dataControllerContactMail} />
              <InputField title='Contact phone' inputRef={dataControllerContactPhoneRef} labelSize='w-48' placeholder='Phone to contact the controller.' defaultValue={initialValue?.dataControllerContactPhone} />
              <InputField title='Identity' inputRef={dataControllerIdentityRef} labelSize='w-48' placeholder="Provide the identity of the controller, or controller's representative." defaultValue={initialValue?.dataControllerIdentity} />
              <InputField title='Data processor' inputRef={dataProcessorRef} labelSize='w-48' placeholder='Describe who is going to process the data.' defaultValue={initialValue?.dataProcessor} />
              <InputField title='Data subprocessors' inputRef={dataSubprocessorsRef} labelSize='w-48' placeholder='Describe sub-processors involved in the data processing, using comma-separated values, e.g. "Sub A, Sub B, Sub C".' defaultValue={initialValue?.dataSubprocessors?.join(', ')} />
              <InputField title='Consent withdraw' inputRef={consentWithdrawRef} labelSize='w-48' placeholder='Describe how the user can withdraw consent to data processing.' defaultValue={initialValue?.consentWithdraw} />
            </Section>

            <Section heading='DPO Information' border='none'>
              <InputField title='DPO Address' inputRef={dpoContactAddressRef} labelSize='w-48' placeholder='Physical address to contact the DPO.' defaultValue={initialValue?.dpoContactAddress} />
              <InputField title='DPO Mail' inputRef={dpoContactMailRef} labelSize='w-48' placeholder='Mail address to contact the DPO.' defaultValue={initialValue?.dpoContactMail} />
              <InputField title='DPO Phone' inputRef={dpoContactPhoneRef} labelSize='w-48' placeholder='Phone to contact the DPO.' defaultValue={initialValue?.dpoContactPhone} />
            </Section>
          </div>

          {/* footer */}
          <div className="flex items-center justify-end px-6 py-5 border-t border-solid rounded-b border-blueGray-200">
            {typeof customError !== 'undefined' && (
              <span className="flex-1 font-bold text-red-500">
                {customError}
              </span>
            )}
            {status === 'error' && (
              <span className="flex-1 font-bold text-red-500">
                Error submitting Transparency Tags
              </span>
            )}
            {status !== 'loading' && (
              <>
                <Button type='reset' styleType='link' theme='cancel'>Cancel</Button>
                <Button type='submit'>Submit</Button>
              </>
            )}
          </div>
        </form>
      </Box>
    </div>
  );
};

export default TransparencyTagsForm;
