import { MutationStatus } from 'react-query';
import { useGet, usePut } from '../../../hooks/useApi';
import useUser from '../../../hooks/useUser';
import { TransparencyTagsSubmission, TransparencyTags as TransparencyTagsModel } from '../../../types/transparencyTags';
import { deepSnakeToCamel } from '../../../utils/objects';
import LoadingScreen from '../../LoadingScreen';
import TransparencyTagsForm from '../form';

const UpdateTransparencyTags = () => {
  const user = useUser();

  if (!user) return <LoadingScreen />;

  const { data: tags, status: getStatus } = useGet<TransparencyTagsModel>(
    `https://privacy-metrics.easypims.eu/privacy-metrics/${user.domain}/`,
    undefined,
    (res) => deepSnakeToCamel(res) as TransparencyTagsModel,
    false,
  );

  const {
    mutate: submitTags, status: submitStatus,
  } = usePut<TransparencyTagsSubmission['provided_information'], TransparencyTagsModel>(
    `https://privacy-metrics.easypims.eu/privacy-metrics/${user.domain}/provided-information`,
    undefined,
    undefined,
    (res) => deepSnakeToCamel(res) as TransparencyTagsModel,
  );

  const onSubmit = (info: TransparencyTagsSubmission['provided_information']) => {
    submitTags({ body: info });
  };

  let status: MutationStatus = 'idle';
  if (getStatus === 'error' || submitStatus === 'error') status = 'error';
  else if (getStatus === 'success' && submitStatus === 'success') status = 'success';
  else if (getStatus === 'loading' || submitStatus === 'loading') status = 'loading';

  return (
    <TransparencyTagsForm
      user={user}
      onSubmit={onSubmit}
      status={status}
      initialValue={tags?.providedInformation}
    />
  );
};

export default UpdateTransparencyTags;
