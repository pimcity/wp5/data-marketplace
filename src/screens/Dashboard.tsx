import { ArrowCircleRightIcon } from '@heroicons/react/solid';
import { Link } from 'react-router-dom';
import { ClipLoader } from 'react-spinners';

import apiRoutes from '../constants/apiRoutes.json';
import PrimaryCard from '../components/PrimaryCard';
import ClosedOffersTable from '../components/offerComponents/ClosedOffersTable';
import { useGet } from '../hooks/useApi';
import Box from '../components/Box';

interface DashboardCard {
  route: string;
  title: string;
  img?: string;
  footMsg: string;
  footLink: string;
}

const DashboardCard = ({
  route, title, img, footMsg, footLink,
}: DashboardCard) => {
  const {
    data, isLoading, isError,
  } = useGet<number>(route);

  let msg = data !== undefined && !isLoading ? `${data}` : <ClipLoader />;
  if (isError) msg = <h4 className='font-black text-red-500'>Error</h4>;

  return (
    <PrimaryCard
      title={title}
      img={img}
      msg={msg}
      footMsg={(<Link className='flex font-medium text-secondary-500 hover:text-secondary-400' to={footLink}>{footMsg} <ArrowCircleRightIcon className='w-5 ml-1' /></Link>)}
    />
  );
};

const Dashboard = () => (
    <div className='flex flex-col'>
      <div className='flex justify-around'>
        <DashboardCard
          route={apiRoutes.accounts.credits.spent}
          title='Credits Spent'
          footMsg='See Transactions'
          footLink='/transactions'
        />
        <DashboardCard
          route={apiRoutes.accounts.credits.available}
          title='Credits Available'
          footMsg='Create Data Offer'
          footLink='/transactions/create'
        />
      </div>
      <Box containerClassName='mt-8' heading='Recent Closed Transactions'>
        <ClosedOffersTable limit={3} />
      </Box>
    </div>
);

export default Dashboard;
