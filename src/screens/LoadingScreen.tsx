const LoadingScreen = () => (
  <div className="flex items-center justify-center w-full h-full">
    Loading
  </div>
);

export default LoadingScreen;
