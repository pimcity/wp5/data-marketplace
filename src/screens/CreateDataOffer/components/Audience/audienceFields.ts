import genderOptions from '../../../../constants/genders.json';
import countryOptions from '../../../../constants/countries.json';
import educationOptions from '../../../../constants/educations.json';
import jobOptions from '../../../../constants/jobs.json';
import interestOptions from '../../../../constants/interests.json';

const AUDIENCE_FIELDS: Array<AudienceField> = [
  {
    id: 'gender',
    label: 'Gender',
    type: 'categories',
    options: genderOptions,
  },
  {
    id: 'country',
    label: 'Country',
    type: 'categories',
    options: countryOptions,
  },
  {
    id: 'age',
    label: 'Age',
    type: 'numeric',
  },
  {
    id: 'income',
    label: 'Annual Income',
    type: 'numeric',
  },
  {
    id: 'householdMembers',
    label: 'Household Members',
    type: 'numeric',
  },
  {
    id: 'education',
    label: 'Education',
    type: 'categories',
    options: educationOptions,
  },
  {
    id: 'job',
    label: 'Job',
    type: 'categories',
    options: jobOptions,
  },
  {
    id: 'interest',
    label: 'Interest',
    type: 'categories',
    options: interestOptions,
  },
];

export default AUDIENCE_FIELDS;
