interface AudienceNumericField {
  id: string;
  label: string;
  type: 'numeric';
}

interface AudienceCategoryField {
  id: string;
  label: string;
  type: 'categories';
  options: Array<{ id: string; label: string; }>;
}

type AudienceField = AudienceNumericField | AudienceCategoryField;

interface NumericFilter {
  min?: number;
  max?: number;
}

type SelectedValue = NumericFilter | Array<string>;
