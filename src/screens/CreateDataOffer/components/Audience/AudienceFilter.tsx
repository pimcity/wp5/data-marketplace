import Multiselect from 'multiselect-react-dropdown';
import { Audience } from '../../../../types/apiModels';

interface AudienceFilterProps {
  className?: string;
  field?: AudienceField;
  value?: SelectedValue;
  availableFields: Array<AudienceField>;
  onSelectField: (newId: keyof Audience, oldId?: keyof Audience | undefined) => void;
  onSelectValue: (id: keyof Audience, value: SelectedValue) => void;
}

const AudienceFilter = (
  {
    className, field, value, availableFields, onSelectField, onSelectValue,
  }: AudienceFilterProps,
) => (
  <div className={`flex ${className || ''}`}>
    <select
      className="px-3 py-2 mr-4 leading-tight text-gray-700 border rounded appearance-none w-72 focus:outline-none focus:shadow-outline"
      onChange={(e) => onSelectField(
        e.target.value as keyof Audience,
        field?.id as keyof Audience | undefined,
      )}
      value={field?.id || ''}
    >
      {!field && <option value={undefined}></option>}
      {availableFields.map((f) => (
        <option
          key={f.id}
          value={f.id}
        >
          {f.label}
        </option>
      ))}
    </select>

    <div className='flex items-center' style={{ width: '37rem' }}>
      {field && field.type === 'categories' && (
        <Multiselect
          options={field.options}
          selectedValues={field.options.filter(
            (o) => (value as Array<string> | undefined)?.includes(o.id),
          )}
          onSelect={(selectedList: Array<{ id: string }>) => onSelectValue(
            field.id as keyof Audience, selectedList.map((s) => s.id),
          )}
          onRemove={(selectedList: Array<{ id: string }>) => onSelectValue(
            field.id as keyof Audience, selectedList.map((s) => s.id),
          )}
          displayValue="label"
          style={{
            multiselectContainer: {
              width: '37rem',
            },
            searchBox: {
              borderWidth: 1,
              borderRadius: '0.25rem',
              borderColor: 'rgba(229, 231, 235, 1)',
              borderStyle: 'solid',
              backgroundColor: 'white',
            },
          }}
        />
      )}

      {field && field.type === 'numeric' && (
        <>
          <span>From</span>
          <input
            className="flex-1 px-3 py-2 mx-2 leading-tight text-gray-700 border rounded appearance-none focus:outline-none focus:shadow-outline"
            type="number"
            value={(value as NumericFilter | undefined)?.min || ''}
            onChange={(e) => onSelectValue(
              field.id as keyof Audience,
              { min: Number(e.target.value), max: (value as NumericFilter | undefined)?.max },
            )}
          />
          <span>To</span>
          <input
            className="flex-1 px-3 py-2 ml-2 leading-tight text-gray-700 border rounded appearance-none focus:outline-none focus:shadow-outline"
            type="number"
            value={(value as NumericFilter | undefined)?.max || ''}
            onChange={(e) => onSelectValue(
              field.id as keyof Audience,
              { min: (value as NumericFilter | undefined)?.min, max: Number(e.target.value) },
            )}
          />
        </>
      )}
    </div>
  </div>
);

export default AudienceFilter;
