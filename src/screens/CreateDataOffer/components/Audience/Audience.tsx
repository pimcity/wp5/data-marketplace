import { useState } from 'react';
import AUDIENCE_FIELDS from './audienceFields';
import AudienceFilter from './AudienceFilter';
import { Audience as AudienceT } from '../../../../types/apiModels';

const Audience = ({ audience, setAudience }: {
  audience: AudienceT,
  setAudience: React.Dispatch<React.SetStateAction<AudienceT>>
}) => {
  const [selectedFields, setSelectedFields] = useState<Array<keyof AudienceT>>([]);

  const onSelectField = (newId: keyof AudienceT, oldId?: keyof AudienceT) => {
    setSelectedFields((prev) => {
      const excluded = prev.filter((id) => id !== oldId);
      return [
        ...excluded,
        newId,
      ];
    });

    setAudience((prev) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const { [oldId]: _, ...rest } = prev; // eslint-disable-line @typescript-eslint/no-unused-vars
      return {
        ...rest,
        [newId]: null,
      };
    });
  };

  const onSelectValue = (id: keyof AudienceT, value: SelectedValue) => setAudience((prev) => ({
    ...prev,
    [id]: value,
  }));

  return (
    <div>
      {
        selectedFields.map((selectedField) => (
          <AudienceFilter
            key={selectedField}
            className='mb-6'
            field={AUDIENCE_FIELDS.find((field) => field.id === selectedField)}
            value={audience[selectedField]}
            availableFields={AUDIENCE_FIELDS.filter((field) => field.id === selectedField
              || selectedFields.every((id) => field.id !== id))}
            onSelectField={onSelectField}
            onSelectValue={onSelectValue}
          />
        ))
      }
      <AudienceFilter
        availableFields={AUDIENCE_FIELDS.filter((field) => selectedFields.every(
          (id) => field.id !== id,
        ))}
        onSelectField={onSelectField}
        onSelectValue={onSelectValue}
      />
    </div>
  );
};

export default Audience;
