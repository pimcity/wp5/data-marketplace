import { FormEvent, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Multiselect from 'multiselect-react-dropdown';

import Button from '../../components/Button';
import apiRoutes from '../../constants/apiRoutes.json';
import availableDataTypes from '../../constants/dataTypes.json';
import availablePurposes from '../../constants/purposes.json';
import { useGet, usePost } from '../../hooks/useApi';
import { DataOffer, DataOfferSubmission, DataType } from '../../types/apiModels';

import Audience from './components/Audience/Audience';
import Box from '../../components/Box';
import LoadingScreen from '../LoadingScreen';

const CreateDataOffer = () => {
  const history = useHistory();
  const { mutate: createOffer, status, error } = usePost<DataOfferSubmission, DataOffer>(
    apiRoutes.market.buyers.dataOffers.create,
    'newOffer',
    [apiRoutes.market.buyers.dataOffers.open, apiRoutes.market.buyers.dataOffers.closed],
  );

  const [audience, setAudience] = useState<DataOfferSubmission['audience']>({});
  const [dataTypes, setDataTypes] = useState<DataOfferSubmission['dataTypes']>([]);
  const [purpose, setPurpose] = useState<DataOfferSubmission['purpose']>('commercial-purpose');
  const [offerBudget, setOfferBudget] = useState<DataOfferSubmission['offerBudget']>(0);
  const [terms, setTerms] = useState<DataOfferSubmission['termsAndConditions']>('');

  const { data: priceData } = useGet<{pricePerIndividual: number}>(
    `${apiRoutes.market.buyers.price}?audience=${encodeURIComponent(JSON.stringify(audience))}&dataTypes=${encodeURIComponent(JSON.stringify(dataTypes))}`,
  );
  const price = priceData ? priceData.pricePerIndividual : undefined;

  const reset = () => history.push('/transactions');

  const submit = (e: FormEvent) => {
    e.preventDefault();
    if (dataTypes.length === 0) {
      return;
    }
    createOffer({
      body: {
        audience,
        dataTypes,
        purpose,
        termsAndConditions: terms,
        offerBudget,
      },
    });
  };

  useEffect(() => {
    if (status === 'success') history.push('/transactions');
  }, [status]);

  if (status === 'loading') return <LoadingScreen />;

  return (
    <div className='flex flex-col'>
      {!!error && <div className='mb-4 font-bold text-center text-red-500'>{error.message}</div>}
      <Box heading='Create Data Offer' contentClassName=''>
        <form onReset={reset} onSubmit={submit} className='flex flex-col'>
          <div className='flex flex-col flex-1 px-6 py-5'>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-bold text-gray-700">
                Audience
              </label>
              <Audience audience={audience} setAudience={setAudience} />
            </div>

            <div className="flex mb-6">
              <div className='mr-4 w-72'>
                <label className="block mb-2 text-sm font-bold text-gray-700">
                  Data
                </label>
                <Multiselect
                  options={availableDataTypes}
                  selectedValues={availableDataTypes.filter(
                    (dt) => dataTypes.includes(dt.id as DataType),
                  )}
                  onSelect={(selectedList: Array<{ id: string }>) => setDataTypes(
                    selectedList.map((s) => s.id as DataType),
                  )}
                  onRemove={(selectedList: Array<{ id: string }>) => setDataTypes(
                    selectedList.map((s) => s.id as DataType),
                  )}
                  displayValue="label"
                  style={{
                    multiselectContainer: {
                      width: '18rem',
                    },
                    searchBox: {
                      borderWidth: 1,
                      borderRadius: '0.25rem',
                      borderColor: 'rgba(229, 231, 235, 1)',
                      borderStyle: 'solid',
                      backgroundColor: 'white',
                    },
                  }}
                />
              </div>

              <div className='mr-4 w-72'>
                <label className="block mb-2 text-sm font-bold text-gray-700">
                  Purpose
                </label>
                <select
                  className="w-full px-3 py-2 mr-4 leading-tight text-gray-700 border rounded appearance-none focus:outline-none focus:shadow-outline"
                  onChange={(e) => setPurpose(e.target.value as DataOfferSubmission['purpose'])}
                  required
                  value={purpose}
                >
                  {availablePurposes.map((p) => (
                    <option key={p.id} value={p.id}>{p.label}</option>
                  ))}
                </select>
              </div>
            </div>

            <div className="flex mb-6">
              <div className="mr-4 w-72">
                <label className="block mb-2 text-sm font-bold text-gray-700">
                  Offer Budget
                </label>
                <input
                  className="w-full px-3 py-2 leading-tight text-gray-700 border rounded appearance-none focus:outline-none focus:shadow-outline"
                  type="number"
                  value={offerBudget}
                  onChange={(e) => setOfferBudget(Number(e.target.value))}
                  required
                  min={10}
                />
              </div>

              <div className='mr-4 w-72'>
                <label className="block mb-2 text-sm font-bold text-gray-700">
                  Price per audience member
                </label>
                <label className="block py-2 leading-tight">
                  {price ? `${price.toFixed(2)} credits` : '-'}
                </label>
              </div>

              <div className="w-72">
                <label className="block mb-2 text-sm font-bold text-gray-700">
                  Max number of users fitting in budget
                </label>
                <label className="block py-2 leading-tight">
                  {price && price > 0 ? Math.floor(offerBudget / price) : 0}
                </label>
              </div>
            </div>

            <div>
              <label className="block mb-2 text-sm font-bold text-gray-700">
                Legal Notice for Data Usage
              </label>
              <label className="block mb-2 text-sm text-gray-500">
                Please, include a legal notice specifying what will the data be used for,
                how will be processed and for how long.
              </label>
              <textarea
                rows={7}
                className="w-full px-3 py-2 leading-tight text-gray-700 border rounded appearance-none focus:outline-none focus:shadow-outline"
                value={terms}
                onChange={(e) => setTerms(e.target.value)}
                required
              />
            </div>
          </div>
          {/* footer */}
          <div className="flex items-center justify-end px-6 py-5 border-t border-solid rounded-b border-blueGray-200">
            <Button type='reset' styleType='link' theme='cancel'>Cancel</Button>
            <Button type='submit'>Create</Button>
          </div>
        </form>
      </Box>
    </div>
  );
};

export default CreateDataOffer;
