import { DetailedHTMLProps } from 'react';

const Card = (
  { children, ...props }: DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
) => (
  <div
    {...props}
    className={`shadow-lg ${props.className || ''}`}
    style={{ backgroundColor: '#F6F8F3', border: '1px dashed #e4e6ef', ...props.style }}
  >
    {children}
  </div>
);

export default Card;
