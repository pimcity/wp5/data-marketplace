import { useHistory, useLocation } from 'react-router-dom';

import logo from '../images/EasyPIMS.png';

import iconStyles from './icon.module.css';

export interface MenuOption {
  label: string;
  icon?: React.FunctionComponent<React.SVGProps<SVGSVGElement> & { title?: string | undefined; }>;
  to?: string;
  onClick?: () => void;
}

interface LeftBarProps {
  menuOptions: Array<MenuOption>;
  footerOptions?: Array<MenuOption>;
}

const getActiveClass = (currentPath: string, target?: string) => (target && currentPath.startsWith(target) ? 'bg-neutral-900' : '');

const Option = ({
  label, icon: Icon, to, onClick,
}: MenuOption) => {
  const { pathname } = useLocation();
  const history = useHistory();

  return (
    <button
      className={`w-full pl-6 pr-2 py-2 flex items-center text-white hover:bg-neutral-900 ${getActiveClass(pathname, to)}`}
      onClick={() => {
        if (to) history.push(to);
        if (onClick) onClick();
      }}
    >
      {Icon && <Icon className={`w-5 h-5 mr-3 ${iconStyles.icon}`} />}
      <label className='cursor-pointer'>{label}</label>
    </button>
  );
};

const LeftBar = ({ menuOptions, footerOptions }: LeftBarProps) => (
    <div className='relative flex flex-col items-center w-64 h-full pt-2 pb-4 text-sm text-center bg-neutral-800'>
      <img className="h-auto mb-6 w-14" src={logo} />
      {menuOptions.map((option) => <Option key={option.label} {...option} />)}
      {
        footerOptions && (
          <div className='absolute w-full bottom-1'>
            {footerOptions.map((option) => <Option key={option.label} {...option} />)}
          </div>
        )
      }
    </div>
);

export default LeftBar;
