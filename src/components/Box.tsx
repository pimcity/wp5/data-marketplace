import { ReactNode } from 'react';

interface BoxProps {
  heading?: string | {
    left?: string | ReactNode;
    right?: string | ReactNode;
  };
  children?: ReactNode;
  containerClassName?: string;
  contentClassName?: string;
}

const Heading = ({ heading }: { heading?: string | ReactNode }) => {
  if (typeof heading === 'string') return (<h2 className='leading-9'>{heading}</h2>);
  return <div>{heading}</div>;
};

const Box = ({
  children, heading, containerClassName, contentClassName, ...props
}: BoxProps) => (
  <div
    {...props}
    className={`bg-white border border-gray-100 rounded-lg shadow-lg ${containerClassName || ''}`}
  >
    {heading && (
      <div className='flex items-center justify-between px-6 py-4 font-medium border-b border-gray-100 text-md'>
        {typeof heading === 'string' && <Heading heading={heading} />}
        {typeof heading !== 'string' && <Heading heading={heading.left} />}
        {typeof heading !== 'string' && <Heading heading={heading.right} />}
      </div>
    )}
    <div className={typeof contentClassName === 'undefined' ? 'px-6 py-5' : contentClassName }>
      {children}
    </div>
  </div>
);

export default Box;
