import { ReactNode } from 'react';

interface SectionProps {
  heading?: string;
  className?: string;
  border?: 'top' | 'bottom' | 'none';
  children?: ReactNode;
}

const borders = {
  top: 'border-t border-solid border-blueGray-200',
  bottom: 'border-b border-solid border-blueGray-200 pb-3',
  none: '',
};

const Section = ({
  heading, className, border = 'bottom', children,
}: SectionProps) => (
  <div className={`${className || ''} ${borders[border]} pt-6`}>
    {heading && <h3 className='mb-6 text-sm font-bold'>{heading}</h3>}
    {children}
  </div>
);

export default Section;
