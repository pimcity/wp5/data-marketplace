import { Audience, NumberFilter } from '../../types/apiModels';

const AudienceValue = ({ value }: { value: NumberFilter | Array<string> }) => {
  if (Array.isArray(value)) return <span>{value.join(', ')}</span>;
  if (value && value.min && value.max) return <span>{value.min} - {value.max}</span>;
  if (value && value.min) return <span>At least {value.min}</span>;
  if (value && value.max) return <span>Atmost {value.max}</span>;
  return <></>;
};

const AudienceComp = ({ audience }: { audience: Audience }) => (
  <div>{Object.entries(audience).map(
    ([key, value]) => (
      <div key={key}>
        <strong>{key[0].toUpperCase() + key.substring(1)}:</strong> <AudienceValue value={value} />
      </div>
    ),
  )}</div>
);

export default AudienceComp;
