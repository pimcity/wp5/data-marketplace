/* eslint-disable react/display-name */
import {
  Audience, DataOffer, DataType, OfferStatus, Purpose,
} from '../../types/apiModels';
import { kebabToText } from '../../utils/words';

import Actions from './Actions';
import AudienceComp from './Audience';
import CreatedAt from './CreatedAt';
import DataTypes from './DataTypes';
import StatusStepper from './StatusStepper';

const displayers: Record<
  keyof DataOffer,
  ((value: any, row: DataOffer) => string | JSX.Element) | undefined
> = {
  createdAt: (createdAt: number) => <CreatedAt date={createdAt} />,
  audience: (audience: Audience) => <AudienceComp audience={audience} />,
  dataTypes: (dataTypes: Array<DataType>) => <DataTypes dataTypes={dataTypes} />,
  purpose: (purpose: Purpose) => <span>{kebabToText(purpose)}</span>,
  offerBudget: (offerBudget: number, { creditsSpent }) => (
    <span>{creditsSpent} / {offerBudget}</span>
  ),
  id: (id: string, row: DataOffer) => <Actions id={id} status={row.status} />,
  status: (status: OfferStatus) => <StatusStepper status={status} />,
  companyId: undefined,
  numberOfUsers: undefined,
  termsAndConditions: undefined,
  creditsSpent: undefined,
};

export default displayers;
