import AccountBalanceWalletRoundedIcon from '@mui/icons-material/AccountBalanceWalletRounded';
import AssignmentTurnedInRoundedIcon from '@mui/icons-material/AssignmentTurnedInRounded';
import AttachMoneyRoundedIcon from '@mui/icons-material/AttachMoneyRounded';
import DownloadRoundedIcon from '@mui/icons-material/DownloadRounded';
import GroupRoundedIcon from '@mui/icons-material/GroupRounded';

import { OfferStatus } from '../../types/apiModels';
import Stepper from '../Stepper';

type VisualStatus = 'getting-price' | 'getting-consents' | 'getting-profiles' | 'getting-watermarked-data' | 'updating-accounts' | 'done';

const statusMap: Record<OfferStatus, VisualStatus> = {
  initial: 'getting-price',
  'getting-price-and-size': 'getting-price',
  'getting-consents': 'getting-consents',
  'getting-profiles': 'getting-profiles',
  'applying-audience-filters': 'getting-profiles',
  'fitting-audience-in-budget': 'getting-profiles',
  'getting-data-packages': 'getting-watermarked-data',
  'watermarking-data': 'getting-watermarked-data',
  'updating-accounts': 'updating-accounts',
  'updating-offer': 'updating-accounts',
  'data-available': 'done',
  failed: 'done',
};

const steps = [
  {
    id: 'getting-price',
    label: 'Calculating Price',
    icon: <AttachMoneyRoundedIcon />,
  },
  {
    id: 'getting-consents',
    label: 'Fetching Consents',
    icon: <AssignmentTurnedInRoundedIcon />,
  },
  {
    id: 'getting-profiles',
    label: 'Filtering Profiles',
    icon: <GroupRoundedIcon />,
  },
  {
    id: 'getting-watermarked-data',
    label: 'Getting Data',
    icon: <DownloadRoundedIcon />,
  },
  {
    id: 'updating-accounts',
    label: 'Updating Accounts',
    icon: <AccountBalanceWalletRoundedIcon />,
  },
];

const StatusStepper = ({ status }: { status: OfferStatus }) => (
  <Stepper steps={steps} activeStep={statusMap[status]} />
);

export default StatusStepper;
