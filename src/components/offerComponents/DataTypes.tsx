import dataTypesDict from '../../constants/dataTypes.json';
import { DataType } from '../../types/apiModels';

const DataTypes = ({ dataTypes }: { dataTypes: Array<DataType> }) => (
  <div>
    {dataTypes.map((id) => (
      <p key={id}>{dataTypesDict.find((v) => v.id === id)?.label}</p>
    ))}
  </div>
);

export default DataTypes;
