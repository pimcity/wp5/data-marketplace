import { useEffect, useState } from 'react';
import apiRoutes from '../../constants/apiRoutes.json';
import { useGet } from '../../hooks/useApi';
import { DataOffer } from '../../types/apiModels';
import { downloadFile } from '../../utils/file';

import Button from '../Button';

const DownloadData = ({ id, onFinish }: { id: string; onFinish: () => void }) => {
  const { data } = useGet(`${apiRoutes.market.buyers.dataOffers.download}/${id}/data`);

  useEffect(() => {
    if (data) {
      downloadFile(data, `${id}-data.json`);
      onFinish();
    }
  }, [data]);

  return <span>Loading...</span>;
};

const Actions = ({ id, status }: { id: string; status: DataOffer['status'] }) => {
  const [download, setDownload] = useState<boolean>(false);
  return (
    <div className='py-2'>
      {status === 'data-available' && (
        <Button className='justify-center w-full' onClick={() => setDownload(true)}>
          {download ? <DownloadData id={id} onFinish={() => setDownload(false)} /> : 'Download Data'}
        </Button>
      )}
    </div>
  );
};

export default Actions;
