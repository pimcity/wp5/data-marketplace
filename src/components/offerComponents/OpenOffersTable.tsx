import apiRoutes from '../../constants/apiRoutes.json';
import { useGet } from '../../hooks/useApi';
import { DataOffer } from '../../types/apiModels';

import Table from '../Table';
import displayers from './displayers';

const OpenOffersTable = ({ limit }: { limit?: number }) => {
  const { data, isLoading } = useGet<Array<DataOffer>>(apiRoutes.market.buyers.dataOffers.open);
  return (
    <Table
      isLoading={isLoading}
      data={data?.slice(0, limit)}
      sort={(a, b) => b.createdAt - a.createdAt}
      displayFns={displayers}
      columns={[
        { header: 'createdAt', label: 'Date', width: 120 },
        { header: 'audience', label: 'Audience', width: 164 },
        { header: 'dataTypes', label: 'Data', width: 90 },
        { header: 'purpose', label: 'Purpose', width: 90 },
        { header: 'status', label: 'Status', className: 'py-2' },
      ]}
    />
  );
};

export default OpenOffersTable;
