const CreatedAt = ({ date }: { date: number }) => (
  <span>{
    (new Date(date)).toISOString().substring(0, 10)
  }</span>
);

export default CreatedAt;
