import apiRoutes from '../../constants/apiRoutes.json';
import { useGet } from '../../hooks/useApi';
import { DataOffer } from '../../types/apiModels';

import Table from '../Table';
import displayers from './displayers';

const ClosedOffersTable = ({ limit }: { limit?: number }) => {
  const { data, isLoading } = useGet<Array<DataOffer>>(apiRoutes.market.buyers.dataOffers.closed);
  return (
    <Table
      isLoading={isLoading}
      data={data}
      sort={(a, b) => b.createdAt - a.createdAt}
      limit={limit}
      displayFns={displayers}
      columns={[
        { header: 'createdAt', label: 'Date', width: 120 },
        { header: 'audience', label: 'Audience', width: 200 },
        { header: 'dataTypes', label: 'Data', width: 164 },
        { header: 'purpose', label: 'Purpose', width: 90 },
        { header: 'offerBudget', label: 'Budget Spent/Total', width: 80 },
        { header: 'numberOfUsers', label: 'Number of Users', width: 80 },
        { header: 'id', label: 'Actions', width: 90 },
      ]}
    />
  );
};

export default ClosedOffersTable;
