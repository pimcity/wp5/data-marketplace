import { ReactElement } from 'react';
import Card from './Card';

interface PrimaryCardProps {
  title: string;
  img?: string;
  msg: string | ReactElement;
  footMsg?: string | ReactElement;
}

const FootMsg = ({ footMsg }: { footMsg?: string | ReactElement }) => {
  if (!footMsg) return <></>;
  return typeof footMsg === 'string' ? (<h4>{footMsg}</h4>) : footMsg;
};

const PrimaryCard = ({
  title, img, msg, footMsg,
}: PrimaryCardProps) => (
  <Card className='flex flex-col items-center p-4 w-60'>
    <h2 className='text-lg font-semibold'>{title}</h2>
    {img && <img className="flex-1 w-12 h-auto my-5" src={img} />}
    <div className='py-6'>
      {
        typeof msg === 'string'
          ? <h3 className='text-5xl font-semibold text-primary-600'>{msg}</h3>
          : msg
      }
    </div>
    {footMsg && (
      <div className='h-6 mt-2'>
        <FootMsg footMsg={footMsg} />
      </div>
    )}
  </Card>
);

export default PrimaryCard;
