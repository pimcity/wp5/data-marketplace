import { Fragment, ReactNode } from 'react';
import { Menu, Transition } from '@headlessui/react';

const UserMenuHead = () => (
  <>
    <span className="sr-only">Open user menu</span>
    <span className="inline-block w-8 h-8 overflow-hidden bg-gray-100 rounded-full">
      <svg className="w-full h-full text-gray-800" fill="currentColor" viewBox="0 0 24 24">
        <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
      </svg>
    </span>
  </>
);

interface FloatingMenuProps {
  menuHead?: ReactNode;
  menuOptions: Array<{
    label: string;
    onClick: () => void;
  }>;
}

const FloatingMenu = ({ menuHead = UserMenuHead, menuOptions }: FloatingMenuProps) => (
    <Menu as="div" className="relative ml-3">
      {({ open }) => (
        <>
          <div>
            <Menu.Button className="flex items-center max-w-xs text-sm bg-white rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500">
              {menuHead}
            </Menu.Button>
          </div>
          <Transition
            show={open}
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items
              static
              className="absolute right-0 z-10 w-48 py-1 mt-2 bg-white shadow-lg origin-top-right rounded-md ring-1 ring-black ring-opacity-5 focus:outline-none"
            >
              {menuOptions.map((option) => (
                <Menu.Item key={option.label}>
                  {() => (
                    <button
                      onClick={ () => option.onClick()}
                      className='block w-full px-4 py-2 text-sm text-gray-700 hover:bg-gray-100'
                    >
                      {option.label}
                    </button>
                  )}
                </Menu.Item>
              ))}
            </Menu.Items>
          </Transition>
        </>
      )}
    </Menu>
);

export default FloatingMenu;
