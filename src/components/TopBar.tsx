const TopBar = ({ title }: { title: string }) => (
    <h1 className='flex items-center justify-between w-full py-3 pl-8 text-lg font-semibold shadow-lg' style={{ backgroundColor: '#f5f8fa' }}>
      {title}
    </h1>
);

export default TopBar;
