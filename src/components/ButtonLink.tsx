import { Link } from 'react-router-dom';
import Button, { ButtonProps } from './Button';

const ButtonLink = ({ to, children, ...props } : ButtonProps & { to: string }) => (
  <Button {...props}>
    <Link to={to}>{children}</Link>
  </Button>
);

export default ButtonLink;
