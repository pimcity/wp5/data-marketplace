import { ReactNode } from 'react';

export interface HorizontalFieldProps {
  name: string;
  children?: ReactNode;
  labelSize?: string;
  className?: string;
}

const HorizontalField = ({
  name, children, labelSize = 'w-32', className,
}: HorizontalFieldProps) => (
  <div className={`flex items-center mb-4 ${className || ''}`}>
    <label className={`${labelSize} mr-6 text-sm font-medium text-gray-400`}>
      {name}
    </label>
    {children}
  </div>
);

export default HorizontalField;
