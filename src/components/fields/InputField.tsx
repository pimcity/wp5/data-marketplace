import { DetailedHTMLProps, RefObject } from 'react';
import HorizontalField from './HorizontalField';

interface InputFieldProps extends DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement
> {
  labelSize?: string;
  className?: string;
  inputRef: RefObject<HTMLInputElement>;
}

const InputField = ({
  inputRef, className, labelSize, ...props
}: InputFieldProps) => (
  <HorizontalField name={props.title || ''} className={className} labelSize={labelSize}>
    <input
      className="flex-1 px-3 py-2 text-sm leading-tight text-gray-700 border rounded appearance-none focus:outline-none"
      {...props}
      ref={inputRef}
    />
  </HorizontalField>
);

export default InputField;
