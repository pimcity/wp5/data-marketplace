import {
  DetailedHTMLProps, RefObject, useEffect, useState,
} from 'react';
import HorizontalField from './HorizontalField';

interface SelectFieldProps extends DetailedHTMLProps<
  React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement
> {
  labelSize?: string;
  className?: string;
  emptyLabel?: string;
  options: Array<string | { id: string, label: string }>;
  selectRef: RefObject<HTMLSelectElement>;
}

const SelectField = ({
  selectRef, className, labelSize, options, emptyLabel, ...props
}: SelectFieldProps) => {
  const [textColor, setTextColor] = useState<string>('text-gray-400');
  useEffect(() => {
    setTextColor(typeof props.defaultValue !== 'undefined' ? 'text-gray-700' : 'text-gray-400');
  }, [props.defaultValue]);

  return (
    <HorizontalField name={props.title || ''} className={className} labelSize={labelSize}>
      <select
        className={`flex-1 px-3 py-2 text-sm leading-tight ${textColor} border rounded appearance-none focus:outline-none`}
        {...props}
        ref={selectRef}
        key={props.defaultValue as string}
        onChange={(e) => setTextColor(e.target.value !== '' ? 'text-gray-700' : 'text-gray-400')}
      >
        {emptyLabel && <option value={''}>{emptyLabel}</option>}
        {options.map((opt) => (typeof opt === 'string'
          ? (<option key={opt} value={opt}>{opt}</option>)
          : (<option key={opt.id} value={opt.id}>{opt.label}</option>)))}
      </select>
    </HorizontalField>
  );
};

export default SelectField;
