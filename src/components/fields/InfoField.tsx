import HorizontalField, { HorizontalFieldProps } from './HorizontalField';

interface InfoFieldProps extends HorizontalFieldProps {
  value: string | string[] | number | boolean;
}

const renderArray = (value: string[]) => (value.length > 0 ? value.join(', ') : 'None');

const InfoField = ({ value, ...props }: InfoFieldProps) => (
  <HorizontalField {...props}>
    <label className="flex-1 text-sm font-bold text-gray-700">
      {Array.isArray(value) ? renderArray(value) : value.toString()}
    </label>
  </HorizontalField>
);

export default InfoField;
