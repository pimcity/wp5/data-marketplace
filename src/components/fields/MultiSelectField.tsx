import Multiselect from 'multiselect-react-dropdown';
import { useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';

import HorizontalField from './HorizontalField';

type Option = string | { id: string, label: string };

interface MultiSelectFieldProps {
  title: string;
  options: Array<Option>;
  value: Array<string>;
  onSelectValue: (value: Array<string>) => void;
  labelSize?: string;
  className?: string;
  placeholder?: string;
}

const MultiSelectField = ({
  title, className, labelSize, value, options, onSelectValue, placeholder = 'Select option',
}: MultiSelectFieldProps) => {
  const div = useRef<Multiselect>(null);
  const usedOptions = options.map((o) => (typeof o === 'string' ? { id: o, label: o } : o));

  useEffect(() => {
    if (div.current) {
      // eslint-disable-next-line react/no-find-dom-node
      const domElement = ReactDOM.findDOMNode(div.current) as Element;
      if (domElement) domElement.className = 'flex-1';
    }
  }, [div.current]);
  return (
    <HorizontalField name={title} className={className} labelSize={labelSize}>
      <Multiselect
        ref={div}
        options={usedOptions}
        selectedValues={usedOptions.filter(
          (o) => (value || []).includes(o.id),
        )}
        onSelect={(selectedList: Array<{ id: string }> = []) => onSelectValue(
          selectedList.map((s) => s.id),
        )}
        onRemove={(selectedList: Array<{ id: string }> = []) => onSelectValue(
          selectedList.map((s) => s.id),
        )}
        displayValue='label'
        placeholder={placeholder}
        style={{
          searchBox: {
            borderWidth: 1,
            borderRadius: '0.25rem',
            borderColor: 'rgba(229, 231, 235, 1)',
            borderStyle: 'solid',
            backgroundColor: 'white',
            paddingLeft: '0.75rem',
            paddingRight: '0.75rem',
            paddingTop: '0.35rem',
            paddingBottom: 0,
            lineHeight: 1.25,
          },
          inputField: {
            marginTop: 0,
            marginBottom: '0.35rem',
            height: 23,
            fontSize: '0.875rem',
            width: '25rem',
          },
          chips: {
            marginBottom: '0.35rem',
          },
          option: {
            fontSize: '0.875rem',
          },
        }}
      />
    </HorizontalField>
  );
};

export default MultiSelectField;
