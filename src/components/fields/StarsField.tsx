import Stars from '../Stars';
import HorizontalField, { HorizontalFieldProps } from './HorizontalField';

interface StarsFieldProps extends HorizontalFieldProps {
  value: number;
}

const StarsField = ({ value, ...props }: StarsFieldProps) => (
  <HorizontalField {...props}>
    <Stars value={value} />
  </HorizontalField>
);

export default StarsField;
