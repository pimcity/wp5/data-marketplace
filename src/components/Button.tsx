export interface ButtonProps extends React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement
> {
  theme?: 'primary' | 'secondary' | 'alternative' | 'cancel';
  styleType?: 'button' | 'link';
}

interface ThemeType {
  all: string;
  primary: string;
  secondary: string;
  alternative: string;
  cancel: string;
}

const themeClasses: { button: ThemeType, link: ThemeType } = {
  button: {
    all: 'font-medium border border-transparent shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 rounded-md',
    primary: 'text-white bg-primary-600 hover:bg-primary-700 focus:ring-primary-600',
    secondary: 'text-white bg-secondary-600 hover:bg-secondary-700 focus:ring-secondary-600',
    alternative: 'text-primary-600 bg-primary-100 hover:text-white hover:bg-primary-600 focus:ring-primary-600',
    cancel: 'text-white bg-red-600 hover:bg-red-700 focus:ring-red-600',
  },
  link: {
    all: 'uppercase font-bold outline-none background-transparent focus:outline-none',
    primary: 'text-primary-600 hover:text-primary-700',
    secondary: 'text-secondary-600 hover:text-secondary-700',
    alternative: 'text-primary-600 hover:text-primary-700',
    cancel: 'text-red-500 hover:text-red-600',
  },
};

const Button = ({
  className, styleType = 'button', theme = 'primary', children, ...props
}: ButtonProps) => {
  const themeType = themeClasses[styleType];

  return (
    <button
      className={`${className || ''} inline-flex items-center justify-center px-3 py-2 text-sm transition duration-300 ease-in-out leading-4 ${themeType.all} ${themeType[theme]}`}
      {...props}
    >
      {children}
    </button>
  );
};

export default Button;
