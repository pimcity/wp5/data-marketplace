import StarIcon from '@mui/icons-material/Star';
import StarOutlineIcon from '@mui/icons-material/StarOutline';
import StarHalfIcon from '@mui/icons-material/StarHalf';

const range = (value: number) => {
  const arr = [];
  for (let i = 0; i < value; i += 1) {
    arr.push(i);
  }
  return arr;
};

const FullStars = ({ value }: { value: number}) => (
  <>
    {range(value).map((i) => <StarIcon key={i} className='w-6 text-yellow-400' />)}
  </>
);

const EmptyStars = ({ value }: { value: number}) => (
  <>
    {range(value).map((i) => <StarOutlineIcon key={i} className='w-6 text-gray-300' />)}
  </>
);

const Stars = ({ value, maxStars = 5 }: { value: number, maxStars?: number }) => {
  const absValue = Math.abs(value);
  const absMaxStars = Math.abs(maxStars);
  return (
    <>
      <FullStars value={Math.min(Math.floor(absValue), absMaxStars)} />
      {absValue <= absMaxStars && Math.floor(absValue) !== absValue && <StarHalfIcon className='w-6 text-yellow-400' />}
      <EmptyStars value={Math.max(absMaxStars - Math.ceil(absValue), 0)} />
    </>
  );
};

export default Stars;
