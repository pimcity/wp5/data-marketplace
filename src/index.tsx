import { StrictMode } from 'react';
import { HashRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import { ReactKeycloakProvider } from '@react-keycloak/web';
import { QueryClient, QueryClientProvider } from 'react-query';
import { StyledEngineProvider } from '@mui/material/styles';

import Routes from './routes';
import reportWebVitals from './reportWebVitals';

import getConfig from './getConfig';
import { ModalProvider } from './contexts/modalContext';
import getKeycloak from './services/keycloak';

import './index.css';

// Create a client
const queryClient = new QueryClient();

ReactDOM.render(
  <StrictMode>
    <HashRouter basename={getConfig(process.env).publicUrl}>
      <ReactKeycloakProvider
        authClient={getKeycloak(process.env)}
        initOptions={{
          onLoad: 'check-sso',
          checkLoginIframe: false,
        }}
      >
        <QueryClientProvider client={queryClient}>
          <StyledEngineProvider injectFirst>
            <ModalProvider>
              <Routes />
            </ModalProvider>
          </StyledEngineProvider>
        </QueryClientProvider>
      </ReactKeycloakProvider>
    </HashRouter>
  </StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
