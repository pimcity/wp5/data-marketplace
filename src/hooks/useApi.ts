// get, put, post, patch, del, head, options

import { useKeycloak } from '@react-keycloak/web';
import { useMutation, useQuery, useQueryClient } from 'react-query';

import {
  ApiError, del, get, MutateType, post, put, patch,
} from '../services/api';

export const useGet = <TData = unknown, TError = unknown>(
  route: string,
  headers?: HeadersInit,
  transformer?: (res: any) => TData,
  retry = true,
) => {
  const { keycloak } = useKeycloak();

  return useQuery<TData, ApiError<TError>>(
    route,
    async () => {
      const res = await get<TData, TError>(route, keycloak?.token, headers);
      return transformer ? transformer(res) : res;
    },
    { retry },
  );
};

export const usePost = <TBody = Record<string, unknown>, TData = TBody, TError = unknown>(
  route: string,
  dataKey?: string | ((res: TData) => string),
  queriesToInvalidate?: string | string[],
  transformer?: (res: any) => TData,
) => {
  const { keycloak } = useKeycloak();
  const queryClient = useQueryClient();

  return useMutation<TData, ApiError<TError>, MutateType<TBody>>(
    async (vars: { body?: TBody, headers?: HeadersInit } = {}) => {
      const res = await post<TData, TError, TBody>(route, keycloak?.token, vars.headers, vars.body);
      return transformer ? transformer(res) : res;
    },
    {
      onSuccess: (data: TData, vars: MutateType<TBody>) => {
        const newData = { ...vars.body, ...data };
        const queryKey = typeof dataKey === 'function' ? dataKey(newData) : dataKey;
        queryClient.setQueryData(queryKey || route, newData);
      },
      onSettled: async () => {
        if (queriesToInvalidate) await queryClient.invalidateQueries(queriesToInvalidate);
      },
    },
  );
};

export const usePut = <TBody = Record<string, unknown>, TData = TBody, TError = unknown>(
  route: string,
  dataKey?: string,
  queriesToInvalidate?: string | string[],
  transformer?: (res: any) => TData,
) => {
  const { keycloak } = useKeycloak();
  const queryClient = useQueryClient();

  return useMutation<TData, ApiError<TError>, MutateType<TBody>>(
    async (vars: { body?: TBody, headers?: HeadersInit } = {}) => {
      const res = await put<TData, TError, TBody>(route, keycloak?.token, vars.headers, vars.body);
      return transformer ? transformer(res) : res;
    },
    {
      onSuccess: (data: TData, vars: MutateType<TBody>) => {
        const newData = Array.isArray(data) ? data : { ...vars.body, ...data };
        queryClient.setQueryData(dataKey || route, newData);
      },
      onSettled: async () => {
        if (queriesToInvalidate) await queryClient.invalidateQueries(queriesToInvalidate);
      },
    },
  );
};

export const usePatch = <TBody = Record<string, unknown>, TData = TBody, TError = unknown>(
  route: string,
  dataKey?: string,
  queriesToInvalidate?: string | string[],
  transformer?: (res: any) => TData,
) => {
  const { keycloak } = useKeycloak();
  const queryClient = useQueryClient();

  return useMutation<TData, ApiError<TError>, MutateType<TBody>>(
    async (vars: { body?: TBody, headers?: HeadersInit } = {}) => {
      const rs = await patch<TData, TError, TBody>(route, keycloak?.token, vars.headers, vars.body);
      return transformer ? transformer(rs) : rs;
    },
    {
      onSuccess: (data: TData, vars: MutateType<TBody>) => {
        const newData = Array.isArray(data) ? data : { ...vars.body, ...data };
        queryClient.setQueryData(dataKey || route, newData);
      },
      onSettled: async () => {
        if (queriesToInvalidate) await queryClient.invalidateQueries(queriesToInvalidate);
      },
    },
  );
};

export const useDelete = <TBody = Record<string, unknown>, TData = TBody, TError = unknown>(
  route: string,
  queriesToInvalidate?: 'all' | string | string[],
) => {
  const { keycloak } = useKeycloak();
  const queryClient = useQueryClient();

  return useMutation<TData, ApiError<TError>, MutateType<TBody>>(
    (
      vars: { body?: TBody, headers?: HeadersInit } = {},
    ) => del<TData, TError, TBody>(route, keycloak?.token, vars.headers, vars.body),
    {
      onSuccess: async () => {
        if (queriesToInvalidate) {
          if (queriesToInvalidate === 'all') {
            queryClient.clear();
          } else {
            await queryClient.invalidateQueries(queriesToInvalidate);
          }
        }
      },
    },
  );
};
