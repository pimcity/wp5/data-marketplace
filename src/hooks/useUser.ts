import { useKeycloak } from '@react-keycloak/web';

export interface User {
  id: string;
  username: string;
  email: string;
  domain: string;
  name: string;
  firstName: string;
  lastName: string;
}

const useUser = () => {
  const { keycloak } = useKeycloak();
  if (!keycloak.idTokenParsed) return undefined;

  return {
    id: keycloak.idTokenParsed.sub,
    username: keycloak.idTokenParsed.preferred_username,
    email: keycloak.idTokenParsed.email,
    domain: (keycloak.idTokenParsed.email as string).split('@')[1],
    name: keycloak.idTokenParsed.name,
    firstName: keycloak.idTokenParsed.given_name,
    lastName: keycloak.idTokenParsed.family_name,
  } as User;
};

export default useUser;
