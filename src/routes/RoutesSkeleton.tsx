import { useEffect } from 'react';
import {
  Redirect,
  Route, RouteComponentProps, useLocation,
} from 'react-router-dom';
import { useKeycloak } from '@react-keycloak/web';

import LoadingScreen from '../screens/LoadingScreen';

import TopBar from '../components/TopBar';
import LeftBar, { MenuOption } from '../components/LeftBar';

interface RouteProps {
  path: string;
  title: string;
  Component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
}

interface RoutesSkeletonProps {
  publicRoutes?: Array<RouteProps>;
  privateRoutes?: Array<RouteProps>;
  menuOptions: Array<MenuOption>;
  footerOptions: Array<MenuOption>;
}

const RoutesSkeleton = ({
  publicRoutes, privateRoutes, menuOptions, footerOptions,
}: RoutesSkeletonProps) => {
  const basePath = (publicRoutes || privateRoutes)?.[0].path;
  if (!basePath) throw new Error('There must be at least one route');

  const { keycloak } = useKeycloak();
  const location = useLocation();

  const pathname = location.pathname.replace(/&.*/g, '');
  const privatePaths = privateRoutes?.map((s) => s.path);

  useEffect(() => {
    if (keycloak) {
      const { authenticated } = keycloak;
      const returnsFromKeycloak = location.pathname.includes('session_state');
      if (!authenticated && !returnsFromKeycloak && privatePaths?.includes(pathname)) {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        keycloak.login();
      }
    }
  }, [keycloak?.authenticated, location.pathname, pathname]);

  if (!keycloak?.authenticated) {
    if (pathname === '/') return <Route path="/" render={() => <Redirect to={basePath} />} />;
    return <div className="w-screen h-screen"><LoadingScreen /></div>;
  }

  return (
    <div className="w-screen h-screen">
      {pathname === '/' && <Route path="/" render={() => <Redirect to={basePath} />} />}
      {publicRoutes?.map(({ path, Component }) => (
        <Route key={path} path={path} component={Component} />
      ))}

      {
        privateRoutes?.map(({ path, title, Component }) => (
          <Route
            key={path}
            render={({ location: loc, ...props }) => (loc.pathname.replace(/&.*/g, '') === path ? (
              <div className="flex w-full h-full">
                <LeftBar menuOptions={menuOptions} footerOptions={footerOptions} />
                <div className="flex flex-col flex-1 h-full">
                  <TopBar title={title} />
                  <div className="flex-1 w-full p-8 overflow-scroll">
                    <Component {...props} location={loc} />
                  </div>
                </div>
              </div>
            ) : null)}
          />
        ))
      }
    </div >
  );
};

export default RoutesSkeleton;
