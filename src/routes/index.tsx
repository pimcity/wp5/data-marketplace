import { useKeycloak } from '@react-keycloak/web';

import { ReactComponent as DashboardIcon } from '../images/gra004.svg';
import { ReactComponent as TransactionsIcon } from '../images/arr034.svg';
import { ReactComponent as SettingsIcon } from '../images/cod001.svg';
import { ReactComponent as SignOutIcon } from '../images/arr092.svg';
import { ReactComponent as PrivacyPolicyIcon } from '../images/gen051.svg';
import { ReactComponent as AboutPimCityIcon } from '../images/gen046.svg';

import CreateDataOffer from '../screens/CreateDataOffer';
import Dashboard from '../screens/Dashboard';
import CreateTransparencyTags from '../screens/TransparencyTags/create';
import EditTransparencyTags from '../screens/TransparencyTags/update';
import TransparencyTags from '../screens/TransparencyTags/read';
import Transactions from '../screens/Transactions';

import RoutesSkeleton from './RoutesSkeleton';

const Routes = () => {
  const { keycloak } = useKeycloak();

  return (
    <RoutesSkeleton
      privateRoutes={[
        { path: '/dashboard', title: 'Dashboard', Component: Dashboard },
        { path: '/transactions', title: 'Transactions', Component: Transactions },
        { path: '/transactions/create', title: 'Create Data Offer', Component: CreateDataOffer },
        { path: '/transparency-tags', title: 'Transparency Tags', Component: TransparencyTags },
        { path: '/transparency-tags/create', title: 'Create Transparency Tags', Component: CreateTransparencyTags },
        { path: '/transparency-tags/edit', title: 'Edit Transparency Tags', Component: EditTransparencyTags },
      ]}
      menuOptions={[
        { label: 'Dashboard', icon: DashboardIcon, to: '/dashboard' },
        { label: 'Transactions', icon: TransactionsIcon, to: '/transactions' },
        { label: 'Transparency Tags', icon: SettingsIcon, to: '/transparency-tags' },
        { label: 'Sign out', icon: SignOutIcon, onClick: () => keycloak.logout() },
      ]}
      footerOptions={[
        { label: 'Privacy Policy', icon: PrivacyPolicyIcon, onClick: () => window.open('https://www.easypims.com/privacy-policy/', '_blank') },
        { label: 'About PIMCity', icon: AboutPimCityIcon, onClick: () => window.open('https://easypims.eu/', '_blank') },
      ]}
    />
  );
};

export default Routes;
