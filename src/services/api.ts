/* global HeadersInit */
import getConfig from '../getConfig';

const METHODS = ['get', 'put', 'post', 'patch', 'delete', 'head', 'options'];

const request = async <TBody = Record<string, unknown>>(
  type: string,
  route: string,
  token?: string,
  headers?: HeadersInit,
  body?: TBody,
) => {
  let url = route;
  if (!route.startsWith('http')) {
    // `process` must be defined globally. That only happens within a React component.
    const config = getConfig(process.env);
    if (!config.apiUrl) throw new Error('API URL must be defined');
    url = `${config.apiUrl.replace(/\/$/, '')}/${route.replace(/^\//, '')}`;
  }

  const requestHeaders: HeadersInit = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: token ? `Bearer ${token}` : '',
    ...headers,
  };
  return fetch(
    url,
    {
      method: type,
      headers: requestHeaders,
      body: JSON.stringify(body),
    },
  );
};

export type IError = { message: string };

export class ApiError<TError = unknown> extends Error {
  res: Response;

  error: TError;

  constructor(res: Response, error: TError & IError) {
    super(`Code ${res.status}: ${error.message}`);
    this.res = res;
    this.error = error;
  }
}

export type MutateType<TBody> = { body?: TBody };

const httpMethods = METHODS.map((method) => async <
  TData = Record<string, unknown>, TError = unknown, TBody = Record<string, unknown>
>(
  route: string,
  token?: string,
  headers: HeadersInit = {},
  body?: TBody,
) => {
  const res = await request(method.toUpperCase(), route, token, headers, body);
  const resData: TData | TError | undefined = await res.json().catch(() => undefined);

  if (res.status >= 400) {
    const errorData = { ...resData, message: 'No message' } as TError & IError;
    throw new ApiError(res, errorData);
  }

  return resData as TData;
});

/**
 * Call this ONLY within a React Component.
 */
export const [get, put, post, patch, del, head, options] = httpMethods;
