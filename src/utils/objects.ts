import { snakeToCamelCase } from './words';

export const isObject = (value: any) => typeof value === 'object' && !Array.isArray(value) && value !== null;

export const deepSnakeToCamel = (obj: { [key: string]: any }): any => Object.entries(obj).reduce(
  (acc, [key, value]) => ({
    ...acc,
    [snakeToCamelCase(key)]: isObject(value) ? deepSnakeToCamel(value) : value,
  }),
  {},
);
