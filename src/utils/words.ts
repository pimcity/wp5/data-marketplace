export const kebabToText = (str: string) => str[0].toUpperCase() + str.slice(1).replace(/-([a-zA-Z])/g, (v) => ` ${v.slice(1).toUpperCase()}`);

export const snakeToCamelCase = (str: string) => str.replace(/_([a-zA-Z])/g, (v) => v.slice(1).toUpperCase());
