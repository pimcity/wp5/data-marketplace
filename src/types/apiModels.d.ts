export type Gender = 'male' | 'female' | 'other';
export type Country = 'AF' | 'AX' | 'AL' | 'DZ' | 'AS' | 'AD' | 'AO' | 'AI' | 'AQ' | 'AG' | 'AR' | 'AM' | 'AW' | 'AU' | 'AT' | 'AZ' | 'BS' | 'BH' | 'BD' | 'BB' | 'BY' | 'BE' | 'BZ' | 'BJ' | 'BM' | 'BT' | 'BO' | 'BQ' | 'BA' | 'BW' | 'BV' | 'BR' | 'IO' | 'BN' | 'BG' | 'BF' | 'BI' | 'KH' | 'CM' | 'CA' | 'CV' | 'KY' | 'CF' | 'TD' | 'CL' | 'CN' | 'CX' | 'CC' | 'CO' | 'KM' | 'CG' | 'CD' | 'CK' | 'CR' | 'CI' | 'HR' | 'CU' | 'CW' | 'CY' | 'CZ' | 'DK' | 'DJ' | 'DM' | 'DO' | 'EC' | 'EG' | 'SV' | 'GQ' | 'ER' | 'EE' | 'ET' | 'FK' | 'FO' | 'FJ' | 'FI' | 'FR' | 'GF' | 'PF' | 'TF' | 'GA' | 'GM' | 'GE' | 'DE' | 'GH' | 'GI' | 'GR' | 'GL' | 'GD' | 'GP' | 'GU' | 'GT' | 'GG' | 'GN' | 'GW' | 'GY' | 'HT' | 'HM' | 'VA' | 'HN' | 'HK' | 'HU' | 'IS' | 'IN' | 'ID' | 'IR' | 'IQ' | 'IE' | 'IM' | 'IL' | 'IT' | 'JM' | 'JP' | 'JE' | 'JO' | 'KZ' | 'KE' | 'KI' | 'KP' | 'KR' | 'KW' | 'KG' | 'LA' | 'LV' | 'LB' | 'LS' | 'LR' | 'LY' | 'LI' | 'LT' | 'LU' | 'MO' | 'MK' | 'MG' | 'MW' | 'MY' | 'MV' | 'ML' | 'MT' | 'MH' | 'MQ' | 'MR' | 'MU' | 'YT' | 'MX' | 'FM' | 'MD' | 'MC' | 'MN' | 'ME' | 'MS' | 'MA' | 'MZ' | 'MM' | 'NA' | 'NR' | 'NP' | 'NL' | 'NC' | 'NZ' | 'NI' | 'NE' | 'NG' | 'NU' | 'NF' | 'MP' | 'NO' | 'OM' | 'PK' | 'PW' | 'PS' | 'PA' | 'PG' | 'PY' | 'PE' | 'PH' | 'PN' | 'PL' | 'PT' | 'PR' | 'QA' | 'RE' | 'RO' | 'RU' | 'RW' | 'BL' | 'SH' | 'KN' | 'LC' | 'MF' | 'PM' | 'VC' | 'WS' | 'SM' | 'ST' | 'SA' | 'SN' | 'RS' | 'SC' | 'SL' | 'SG' | 'SX' | 'SK' | 'SI' | 'SB' | 'SO' | 'ZA' | 'GS' | 'SS' | 'ES' | 'LK' | 'SD' | 'SR' | 'SJ' | 'SZ' | 'SE' | 'CH' | 'SY' | 'TW' | 'TJ' | 'TZ' | 'TH' | 'TL' | 'TG' | 'TK' | 'TO' | 'TT' | 'TN' | 'TR' | 'TM' | 'TC' | 'TV' | 'UG' | 'UA' | 'AE' | 'GB' | 'US' | 'UM' | 'UY' | 'UZ' | 'VU' | 'VE' | 'VN' | 'VG' | 'VI' | 'WF' | 'EH' | 'YE' | 'ZM' | 'ZW';
export type Education = 'none' | 'primary' | 'secondary' | 'vocational' | 'bachelor' | 'master' | 'doctorate-or-higher';
export type Job = 'accounting' | 'airlines-aviation' | 'alternative-dispute-resolution' | 'alternative-medicine' | 'animation' | 'apparel-fashion' | 'architecture-planning' | 'arts-crafts' | 'automotive' | 'aviation-aerospace' | 'banking-mortgage' | 'biotechnology-greentech' | 'broadcast-media' | 'building-materials' | 'business-supplies-equipment' | 'capital-markets-hedge-fund-private-equity' | 'chemicals' | 'civic-social-organization' | 'civil-engineering' | 'commercial-real-estate' | 'computer-games' | 'computer-hardware' | 'computer-networking' | 'computer-software-engineering' | 'computer-network-security' | 'construction' | 'consumer-electronics' | 'consumer-goods' | 'consumer-services' | 'cosmetics' | 'dairy' | 'defense-space' | 'design' | 'e-learning' | 'education-management' | 'electrical-electronic-manufacturing' | 'entertainment-movie-production' | 'environmental-services' | 'events-services' | 'executive-office' | 'facilities-services' | 'farming' | 'financial-services' | 'fine-art' | 'fishery' | 'food-production' | 'food-beverages' | 'fundraising' | 'furniture' | 'gambling-casinos' | 'glass-ceramics-concrete' | 'government-administration' | 'government-relations' | 'graphic-design-web-design' | 'health-fitness' | 'higher-education-acadamia' | 'hospital-health-care' | 'hospitality' | 'human-resources-hr' | 'import-export' | 'individual-family-services' | 'industrial-automation' | 'information-services' | 'information-technology-it' | 'insurance' | 'international-affairs' | 'international-trade-development' | 'internet' | 'investment-banking-venture' | 'investment-management-hedge-fund-private-equity' | 'judiciary' | 'law-enforcement' | 'law-practice-law-firms' | 'legal-services' | 'legislative-office' | 'leisure-travel' | 'library' | 'logistics-procurement' | 'luxury-goods-jewelry' | 'machinery' | 'management-consulting' | 'maritime' | 'market-research' | 'marketing-advertising-sales' | 'mechanical-or-industrial-engineering' | 'media-production' | 'medical-equipment' | 'medical-practice' | 'mental-health-care' | 'military-industry' | 'mining-metals' | 'motion-pictures-film' | 'museums-institutions' | 'music' | 'nanotechnology' | 'newspapers-journalism' | 'non-profit-volunteering' | 'oil-energy-solar-greentech' | 'online-publishing' | 'other-industry' | 'outsourcing-offshoring' | 'package-freight-delivery' | 'packaging-containers' | 'paper-forest-products' | 'performing-arts' | 'pharmaceuticals' | 'philanthropy' | 'photography' | 'plastics' | 'political-organization' | 'primary-secondary-education' | 'printing' | 'professional-training' | 'program-development' | 'public-relations-pr' | 'public-safety' | 'publishing-industry' | 'railroad-manufacture' | 'ranching' | 'real-estate-mortgage' | 'recreational-facilities-services' | 'religious-institutions' | 'renewables-environment' | 'research-industry' | 'restaurants' | 'retail-industry' | 'security-investigations' | 'semiconductors' | 'shipbuilding' | 'sporting-goods' | 'sports' | 'staffing-recruiting' | 'supermarkets' | 'telecommunications' | 'textiles' | 'think-tanks' | 'tobacco' | 'translation-localization' | 'transportation' | 'utilities' | 'venture-capital-vc' | 'veterinary' | 'warehousing' | 'wholesale' | 'wine-spirits' | 'wireless' | 'writing-editing';
export type Interest = 'IAB1' | 'IAB2' | 'IAB3' | 'IAB4' | 'IAB5' | 'IAB6' | 'IAB7' | 'IAB8' | 'IAB9' | 'IAB10' | 'IAB11' | 'IAB12' | 'IAB13' | 'IAB14' | 'IAB15' | 'IAB16' | 'IAB17' | 'IAB18' | 'IAB19' | 'IAB20' | 'IAB21' | 'IAB22' | 'IAB23' | 'IAB24' | 'IAB25' | 'IAB26';

export interface NumberFilter {
  min?: number;
  max?: number;
}

export interface Audience {
  gender?: Array<Gender>;
  country?: Array<Country>;
  age?: NumberFilter;
  income?: NumberFilter;
  householdMembers?: NumberFilter;
  education?: Array<Education>;
  job?: Array<Job>;
  interest?: Array<Interest>;
}

export type Purpose = 'commercial-purpose' | 'research';
export type DataType = 'personal-information' | 'browsing-history' | 'location-history' | 'social-networks-data' | 'financial-data' | 'interests';
export type OfferStatus = 'initial' | 'getting-price-and-size' | 'getting-consents' | 'getting-profiles' | 'applying-audience-filters' | 'fitting-audience-in-budget' | 'getting-data-packages' | 'watermarking-data' | 'updating-accounts' | 'updating-offer' | 'data-available' | 'failed';

export interface DataOfferSubmission {
  audience: Audience;
  dataTypes: Array<DataType>;
  purpose: Purpose;
  termsAndConditions: string;
  offerBudget: number;
}

export interface DataOffer extends DataOfferSubmission {
  id: string;
  companyId: string;
  numberOfUsers: number;
  creditsSpent: number;
  createdAt: number;
  status: OfferStatus;
}
