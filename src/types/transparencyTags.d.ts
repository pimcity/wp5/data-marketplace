export interface ProvidedInformation {
  category: string;
  consentWithdraw: string;
  country: string;
  dataController: string;
  dataControllerContactAddress: string;
  dataControllerContactMail: string;
  dataControllerContactPhone: string;
  dataControllerIdentity: string;
  dataForAutomaticDecision: string;
  dataLocation: string;
  dataProcessed: string;
  dataProcessingLegalBasis: string;
  dataProcessingPurposes: Array<string>;
  dataProcessor: string;
  dataRecipients: Array<string>;
  dataRetentionPeriod: string;
  dataSubprocessors: Array<string>;
  dataTransfer: string;
  declaredCompanyName: string;
  dpoContactAddress: string;
  dpoContactMail: string;
  dpoContactPhone: string;
  owner: string;
  userRights: string;
  website: string;
}

export interface Webdata {
  categories: Array<string>;
  companyName: string;
  connectedThirdParties: Array<string>;
  connectedWebsites: Array<string>;
  operatesUnder: string;
  presenceInSecurityLists: Array<string>;
  privacyPolicy: string;
  rankInCategory: number;
  reach: string;
  thirdParty: boolean;
  trackingDevices: Array<string>;
  website: boolean;
}

export interface Scores {
  privacyScore: number;
  securityScore: number;
  transparencyScore: number;
}

export interface TransparencyTags {
  id: string;
  name: string;
  providedInformation: ProvidedInformation;
  webdata: Webdata;
  scores: Scores;
}

export interface ProvidedInformationSubmission {
  category: string;
  'consent_withdraw': string;
  country: string;
  'data_controller': string;
  'data_controller_contact_address': string;
  'data_controller_contact_mail': string;
  'data_controller_contact_phone': string;
  'data_controller_identity': string;
  'data_for_automatic_decision': string;
  'data_location': string;
  'data_processed': string;
  'data_processing_legal_basis': string;
  'data_processing_purposes': Array<string>;
  'data_processor': string;
  'data_recipients': Array<string>;
  'data_retention_period': string;
  'data_subprocessors': Array<string>;
  'data_transfer': string;
  'declared_company_name': string;
  'dpo_contact_address': string;
  'dpo_contact_mail': string;
  'dpo_contact_phone': string;
  owner: string;
  'user_rights': string;
  website: string;
}

export interface TransparencyTagsSubmission {
  identifier: string;
  name: string;
  'provided_information': Partial<ProvidedInformationSubmission>;
  webdata?: any;
  scores?: any;
}
