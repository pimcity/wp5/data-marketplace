#!/bin/sh

NODE_ENV=development yarn

if [ $NODE_ENV = 'production' ]
then
  yarn build
  yarn serve
else
  if [ $DEBUG_MODE ]
  then
    yarn debug
  else
    yarn start
  fi
fi