FROM node:12.20.1-alpine3.10

EXPOSE 3000

VOLUME [ "/opt/frontend" ]

WORKDIR /opt/frontend