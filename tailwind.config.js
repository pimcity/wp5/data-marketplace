module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
           50: 'hsl(83deg 93% 90%)',
          100: 'hsl(202deg 100% 96%)',
          200: 'hsl(83deg 93% 75%)',
          300: 'hsl(83deg 93% 65%)',
          400: '#8eb470',
          500: 'hsl(83deg 93% 45%);',
          600: 'hsl(83deg 93% 36%)',
          700: 'hsl(83deg 93% 25%)',
          800: 'hsl(83deg 93% 15%)',
          900: 'hsl(83deg 93% 5%)',
        },
        secondary: {
           50: 'hsl(216deg 98% 90%)',
          100: 'hsl(216deg 98% 85%)',
          200: 'hsl(216deg 98% 75%)',
          300: 'hsl(216deg 98% 65%)',
          400: 'hsl(216deg 98% 55%)',
          500: 'hsl(216deg 98% 45%);',
          600: 'hsl(216deg 98% 52%)',
          700: 'hsl(216deg 98% 25%)',
          800: 'hsl(216deg 98% 15%)',
          900: 'hsl(216deg 98% 5%)',
        },
        neutral: {
          50: 'hsl(240deg 2% 90%)',
         100: 'hsl(240deg 2% 85%)',
         200: 'hsl(240deg 2% 75%)',
         300: 'hsl(240deg 2% 65%)',
         400: 'hsl(240deg 2% 55%)',
         500: 'hsl(240deg 2% 45%);',
         600: 'hsl(240deg 2% 36%)',
         700: 'hsl(240deg 2% 23%)',
         800: 'hsl(240deg 2% 10%)',
         900: 'hsl(240deg 3% 7%)',
       },
      },
      zIndex: {
        '-10': '-10',
      },
      flex: {
        '2': '2 2 0%',
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
